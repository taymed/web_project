const express = require('express');
const controler = require("../controllers/login.controller")
const router = express.Router();
var access = require('../middleware/token.middlware');



// cette route permet de se coonecter 
router.route('/')
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain'); //on retourne du text
        next(); // on vérifie si il ya correspendance avec la fonction suivante

    })
    .post(controler.login)
    .get(controler.verification)


module.exports = router;

