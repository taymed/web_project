const express = require('express');
const  controler = require("../controllers/livre.controller")
const router = express.Router();


// cette route permet d'afficher tous les livre ou d'en ajouter un
router.route('/')
.all((req ,res, next)=>{
    res.statusCode =200;
    res.setHeader('Content-Type','text/plain'); //on retourne du text
    next(); // on vérifie si il ya correspendance avec la fonction suivante

})
.get(controler.getAll)
.post(controler.addOne)
.delete(controler.deleteAll)

router.route('/administrateur')
.all((req ,res, next)=>{
    res.statusCode =200;
    res.setHeader('Content-Type','text/plain'); //on retourne du text
    next(); // on vérifie si il ya correspendance avec la fonction suivante

})
.get(controler.getAllAdmin)
.post(controler.addOne)




// manipuler un seul élément (Récupérer, modifier, supprimer)
router.route("/:id")
.all((req ,res, next)=>{
    res.statusCode =200;
    res.setHeader('Content-Type','text/plain'); //on retourne du text
    next();

}) 
.get(controler.getOne)
.put(controler.updateOne)
.delete(controler.deleteOne);




module.exports = router;

