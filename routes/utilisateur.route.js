const express = require('express');
const controler = require("../controllers/utilisateur.controller")
const router = express.Router();


// cette route permet d'ajouter un utilisateur  
router.route('/')
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain'); //on retourne du text
        next(); // on vérifie si il ya correspendance avec la fonction suivante

    })
    .post(controler.addOne)

router.route("/test/:email")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain'); //on retourne du text
        next();

    })
    .get(controler.getOneEmail)



// manipuler un seul élément (Récupérer, modifier, supprimer)
router.route("/:id")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain'); //on retourne du text
        next();

    })
    .get(controler.getOne)
    .put(controler.updateOne)
    .delete(controler.deleteOne);




module.exports = router;

