FROM node:14
# création d'un répértoire de travail
WORKDIR /usr/src/app

# récupérer la liste des dépendances
COPY package*.json ./

# installer les dépendances
RUN npm install

# copier les fichiers de l'application
COPY . .

# exposer le port 8080
EXPOSE 8080

# à exécuter au lancement du contenaire
CMD [ "node", "app.js" ]