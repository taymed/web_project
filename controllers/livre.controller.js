const livre = require("../models/livre.model");
var jwt = require('jsonwebtoken');

module.exports = {

    getAll:(req,res,next)=>{

      if (verifyToken(req, res, next)){
        const titre = req.query.titre;
        var condition = titre ? { titre: { $regex: new RegExp(titre), $options: "i" } } : {};

        livre.find(condition)
            .then(data => {
            res.send(data);
            })
            .catch(err => {
            res.status(500).send({
                message:
                err.message || "Some error occurred while retrieving livres"
            });
            });}
            else{
              res.statusCode = 404;
              res.setHeader("content-Type", "application/json");
              res.json({message: "vous fouinez un peu trops à mon gout"});
              return res;
          }
        },

        getAllAdmin:(req,res,next)=>{
          console.log("here")

          if (controleAdmin(req, res, next)){
            const titre = req.query.titre;
            var condition = titre ? { titre: { $regex: new RegExp(titre), $options: "i" } } : {};
    
            livre.find(condition)
                .then(data => {
                res.send(data);
                })
                .catch(err => {
                res.status(500).send({
                    message:
                    err.message || "Some error occurred while retrieving livres"
                });
                });}
                else{
                  res.statusCode = 404;
                  res.setHeader("content-Type", "application/json");
                  res.json({message: "vous fouinez un peu trops à mon gout"});
                  return res;
              }
            },

    addOne:(req,res,next)=>{

      if(controleAdmin(req, res, next)){   
        livre.create(req.body)
        // then s'execute si tou va bien
        .then((livre)=>{
            res.statusCode = 200;
            res.setHeader("content-Type", "application/json");
            res.json(livre);
            // gestion des via la focntion next
        }, err => next(err)
        )
        .catch(err =>{
            res.status(500).send({
              message:
                err.message || "Erreur lors de la création du livre"
            });
          });}
          else{
            res.statusCode = 404;
            res.setHeader("content-Type", "application/json");
            res.json({message: "vous fouinez un peu trops à mon gout"});
            return res;
        }

    },
    
    deleteOne:(req,res,next)=>{ 

      console.log("je rentre ici")
      
      if(controleAdmin(req, res, next)){
        livre.findByIdAndDelete(req.params.id)
      .then((resp) =>{
          res.statusCode = 200;
          res.setHeader("content-Type", "application/json");
          res.json(resp)

      }, err => next(err)
      )
      .catch(err => next(err));}

      else{
        res.statusCode = 404;
        res.setHeader("content-Type", "application/json");
        res.json({message: "vous fouinez un peu trops à mon gout"});
        return res;
    }

    },

    deleteAll:(req,res,next)=>{
      if(controleAdmin(req, res, next)){ 
        livre.deleteMany({})
        .then(data => {
          res.send({
            message: `${data.deletedCount} `
          });
        })
        .catch(err => {
          res.status(500).send({
            message:
              err.message || "Some error occurred while removing all livres."
          });
        });}
        else{
          res.statusCode = 404;
          res.setHeader("content-Type", "application/json");
          res.json({message: "vous fouinez un peu trops à mon gout"});
          return res;
      }

    },

    getOne:(req,res,next)=>{ 
      if(controleAdmin(req, res, next)){
        livre.findById(req.params.id)
        .then((livre)=>{
            res.statusCode = 200;
            res.setHeader("content-Type", "application/json");
            res.json(livre);
            
         }, err => next(err)
         )
         .catch(err => next(err));}
         else{
          res.statusCode = 404;
          res.setHeader("content-Type", "application/json");
          res.json({message: "vous fouinez un peu trops à mon gout"});
          return res;
      }
        
        },

    updateOne:(req,res,next)=>{ 
 
      if(controleAdmin(req, res, next)){
        // le paramettre new force la fonction a renvoyer la nouvelle version d'livre
        livre.findByIdAndUpdate(req.params.id, {$set:req.body},{new:true})
        .then((livre)=>{
            res.status(200).json({
                data: livre,
                message: "Livre was updated successfully."
            
              });
            
         }, err => next(err)
         )
         .catch(err => next(err));}
         else{
          res.statusCode = 404;
          res.setHeader("content-Type", "application/json");
          res.json({message: "vous fouinez un peu trops à mon gout"});
          return res;
      }

    },
  
}

function verifyToken(req, res, next) {
  let token = req.query.token;
  b = false;

  return jwt.verify(token, 'secret_pour_cryptage', function (err) {
      if (err) {
          console.log("Pas d'errur")
          return b = false;
      }
      return b = true;

  });

}

function controleAdmin(req, res, next){
  let token = req.query.token;
  b = false;

  if(token == "admin_token"){
    console.log("houraaa le roi est de retour");
    return b = true;
  }
  else{
    return b;
  }
}