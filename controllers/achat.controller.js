const achat = require("../models/achat.model");
var jwt = require('jsonwebtoken');

module.exports = {

    getAll: (req, res, next) => {

        if(verifyToken(req, res, next)){
        achat.find({}).then((achats) => {
            res.statusCode = 200;
            res.setHeader("content-Type", "application/json");
            res.json(achats);

        }, err => next(err)
        )
            .catch(err => next(err));}
        
        else{
            res.statusCode = 404;
            res.setHeader("content-Type", "application/json");
            res.json({message: "vous fouinez un peu trops à mon gout"});
            return res;
        }
    
    },

    addOne: (req, res, next) => {

        if(verifyToken(req, res, next)){
        achat.create(req.body)
            // then s'execute si tou va bien
            .then((achat) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(achat);
                // gestion des via la focntion next
            }, err => next(err)
            )
            .catch(err => next(err));}

        else{
            res.statusCode = 404;
            res.setHeader("content-Type", "application/json");
            res.json({message: "vous fouinez un peu trops à mon gout"});
            return res;
        }
    },

    deleteOne: (req, res, next) => {

        if(verifyToken(req, res, next)){
        achat.findByIdAndDelete(req.params.id)
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(resp)

            }, err => next(err)
            )
            .catch(err => next(err));}
            else{
                res.statusCode = 404;
                res.setHeader("content-Type", "application/json");
                res.json({message: "vous fouinez un peu trops à mon gout"});
                return res;
            }

    },

    getOne: (req, res, next) => {
        if(verifyToken(req, res, next)){
        achat.findById(req.params.id)
            .then((achat) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(achat);

            }, err => next(err)
            )
            .catch(err => next(err));}
            else{
                res.statusCode = 404;
                res.setHeader("content-Type", "application/json");
                res.json({message: "vous fouinez un peu trops à mon gout"});
                return res;
            }
            
    },

    updateOne: (req, res, next) => {
        if(verifyToken(req, res, next)){

        // le paramettre new force la fonction a renvoyer la nouvelle version d'achat
        achat.findByIdAndUpdate(req.params.id, { $set: req.body }, { new: true })
            .then((achat) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(achat);

            }, err => next(err)
            )
            .catch(err => next(err));}
            else{
                res.statusCode = 404;
                res.setHeader("content-Type", "application/json");
                res.json({message: "vous fouinez un peu trops à mon gout"});
                return res;
            }

    },

    

}

function verifyToken(req, res, next) {
    let token = req.query.token;
    b = false;

    return jwt.verify(token, 'secret_pour_cryptage', function (err) {
        if (err) {
            console.log("Pas d'errur")
            return b = false;
        }
        return b = true;

    });

}