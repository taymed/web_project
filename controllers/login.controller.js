const utilisateur = require("../models/utilisateur.model");
var jwt = require('jsonwebtoken');

module.exports = {

    login: (req, res, next) => {
        let promise = utilisateur.findOne({ email: req.body.email }).exec();
        promise.then(function (doc) {
            if (doc) {
                if (doc.isValid(req.body.password)) {
                    let token = jwt.sign({ nom: doc.nom }, 'secret_pour_cryptage');
                    res.status(200).json(token);
                }
                else {
                    console.log("mot de passe invalide");
                    return res.status(501).json({ message: "mot de passe invalide" });
                }
            }
            else {
                console.log("mot de passe invalide");
                return res.status(501).json({ message: "email innexistant en base" });
            }
        });
        promise.catch(function (err) {
            return res.status(501).json({ message: "une erreure est survenue" });
        });
    },

    verification: (req, res, next) => {

        if (verifyToken(req, res, next)) {

            console.log("dans la partie qu'on veux");
            res.statusCode = 200;
            res.setHeader("content-Type", "application/json");
            res.json({ a: "nihao" });

            return res;
        }
        else {
            console.log("pas dans la partie qu'on veux");
            res.statusCode = 404;
            res.setHeader("content-Type", "application/json");
            res.json({ message: "vous fouinez un peu trops à mon gout" });

            return res;
        }

    }

}

function verifyToken(req, res, next) {
    let token = req.query.token;
    b = false;
    return jwt.verify(token, 'secret_pour_cryptage', function (err) {
        if (err) {
            console.log("Pas d'errur")
            return b = false;
        }
        return b = true;

    });

}