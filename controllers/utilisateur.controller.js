const utilisateur = require("../models/utilisateur.model");
var jwt = require('jsonwebtoken');

module.exports = {

    getAll: (req, res, next) => {
        
        if(verifyToken(req, res, next)){
        utilisateur.find({}).then((utilisateurs) => {
            res.statusCode = 200;
            res.setHeader("content-Type", "application/json");
            res.json(utilisateurs);

        }, err => next(err)
        )
            .catch(err => next(err));}
        else{
            res.statusCode = 404;
            res.setHeader("content-Type", "application/json");
            res.json({message: "vous fouinez un peu trops à mon gout"});
            return res;
        }
    },

    addOne: (req, res, next) => {

        var user = new utilisateur({

            nom: req.body.nom,
            prenom: req.body.prenom,
            dateNaissance: req.body.dateNaissance,
            genre: req.body.genre,
            adresse: req.body.adresse,
            numCarte: req.body.numCarte,
            dateCarte: req.body.dateCarte,
            codeCarte: req.body.codeCarte,
            email: req.body.email,
            password: utilisateur.hashPassword(req.body.password)
        })
        
        let promise = user.save();
        promise.then(function(doc){
            res.statusCode = 201;
            res.setHeader("content-Type", "application/json");
            res.json(doc);
            return res;
        })
        promise.catch(function(err){
            res.statusCode = 501;
            res.setHeader("content-Type", "application/json");
            res.json({message: 'Erreure inscription non réussie !'});
            return res;
        })


    },

    deleteOne: (req, res, next) => {

        if(verifyToken(req, res, next)){
        utilisateur.findByIdAndDelete(req.params.id)
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(resp)

            }, err => next(err)
            )
            .catch(err => next(err));}
            else{
                res.statusCode = 404;
                res.setHeader("content-Type", "application/json");
                res.json({message: "vous fouinez un peu trops à mon gout"});
                return res;
            }

    },

    getOne: (req, res, next) => {
        if(verifyToken(req, res, next)){
        utilisateur.findById(req.params.id)
            .then((utilisateur) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(utilisateur);

            }, err => next(err)
            )
            .catch(err => next(err));}
            else{
                res.statusCode = 404;
                res.setHeader("content-Type", "application/json");
                res.json({message: "vous fouinez un peu trops à mon gout"});
                return res;
            }
    },

    updateOne: (req, res, next) => {
        
        if(verifyToken(req, res, next)){
        // le paramettre new force la fonction a renvoyer la nouvelle version d'utilisateur
        utilisateur.findByIdAndUpdate(req.params.id, { $set: req.body }, { new: true })
            .then((utilisateur) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(utilisateur);

            }, err => next(err)
            )
            .catch(err => next(err));}
            else{
                res.statusCode = 404;
                res.setHeader("content-Type", "application/json");
                res.json({message: "vous fouinez un peu trops à mon gout"});
                return res;
            }

    },

    getOneEmail: (req, res, next) => {
        // if(verifyToken(req, res, next)){
            utilisateur.findOne({email: req.params.email.split("=")[1]})
            .then((utilisateur) => {
                res.statusCode = 200;
                res.setHeader("content-Type", "application/json");
                res.json(utilisateur);

            }, err => next(err)
            )
            // .catch(err => next(err));}
            // else{
            //     res.statusCode = 404;
            //     res.setHeader("content-Type", "application/json");
            //     res.json({message: "vous fouinez un peu trops à mon gout"});
            //     return res;
            // }
    },

}

function verifyToken(req, res, next) {
    let token = req.query.token;
    b = false;

    return jwt.verify(token, 'secret_pour_cryptage', function (err) {
        if (err) {
            console.log("Pas d'errur")
            return b = false;
        }
        return b = true;

    });

}