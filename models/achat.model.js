const mongoose = require('mongoose');
const schema = mongoose.Schema;
const autoIncrement = require("mongoose-auto-increment");

const achatSchema = new schema(
    {
        email: { type: String, required: true },
        total: { type: Number, required: true },
        achats: [{ type: schema.Types.ObjectId, ref: 'livre', required: true }]
    },
    // ajoute date de création et date de Maj
    { collection: "achat", timestamps: true }

);

autoIncrement.initialize(mongoose.connection);
achatSchema.plugin(autoIncrement.plugin, {
  model: "achat", // collection or table name in which you want to apply auto increment
  field: "id", // field of model which you want to auto increment
  startAt: 0, // start your auto increment value from 0
  incrementBy: 1, // incremented by 1
});


module.exports = mongoose.model("achat", achatSchema);