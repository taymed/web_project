const mongoose = require('mongoose');
const schema = mongoose.Schema;
const autoIncrement = require("mongoose-auto-increment");


const livreSchema = new schema( 
   {
      titre: { type: String, required: true },
      auteurs: { type: String, required: true },
      datePublication: { type: Date, required: true },
      genre: {type: String, required:true },
      quatriemePage: { type: String, required: true },
      prix: {type: Number, required: true },
      quantite: {type: Number, required: true }
   },
   // ajoute date de création et date de Maj
   { collection: "livre", timestamps: true }

);

autoIncrement.initialize(mongoose.connection);
livreSchema.plugin(autoIncrement.plugin, {
  model: "livre", // collection or table name in which you want to apply auto increment
  field: "id", // field of model which you want to auto increment
  startAt: 1, // start your auto increment value from 1
  incrementBy: 1, // incremented by 1
});



module.exports = mongoose.model("livre", livreSchema);