const mongoose = require('mongoose');
const schema = mongoose.Schema;
var bcrypt = require('bcrypt');

var utilisateurSchema = new schema(
   {
      nom: { type: String, required: true },
      prenom: { type: String, required: true },
      dateNaissance: { type: String, required: false },
      genre: {type: String, required:false },
      adresse: { type: String, required: true },
      numCarte: {type: String, required: true },
      dateCarte: {type: String, required: true },
      codeCarte: {type: String, required: true },
      email:{type: String, required: true},
      password:{type:String, required: true}
   },
   // ajoute date de création et date de Maj
   { collection: "utilisateur", timestamps: true }

);
utilisateurSchema.statics.hashPassword = function hashPassword(password){
   return bcrypt.hashSync(password,10);
}

utilisateurSchema.methods.isValid = function(hashedpassword){
   return  bcrypt.compareSync(hashedpassword, this.password);
}

module.exports = mongoose.model("utilisateur", utilisateurSchema);